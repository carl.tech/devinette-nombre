# Devinette d'un nombre

Ce projet consiste à créer un mini-jeu de devinette en Python en utilisant Git. Le programme choisit un nombre aléatoire entre 1 et 100. L'utilisateur doit deviner le nombre en moins de 10 essais. L’objectif est d’apprendre les bases de la gestion de versions et se familiariser avec un workflow Git simple.

## Questions du TP

1- Créer un nouveau projet sur Gitlab nommé « Devinette d'un nombre » et le cloner sur votre ordinateur.


2- Créer dans le dossier cloné un fichier « main.py » et écrire le programme du jeu.


3- Propagez les modifications sur le dépôt distant avec un message de commit clair et précis.

![Questions 1,2 et 3](Question1.png)


4- Créez une nouvelle branche « dev ».


5- Développer dans cette branche la possibilité que le programme affiche des indices pour aider l'utilisateur (plus grand, plus petit) et qu’il affiche à la fin après combien d’essais.

![question5 et 4](question5.png)

6- Sauvegarder les modifications sur le dépôt distant.


7- Procéder à la fusion de la banche « dev » avec la branche principale.

![question 7 et 6](question6.png)
